### How to run API:

1. Clone the repo and enter the project folder

```
git clone git@gitlab.com:ivan.teaca/isv3-120-blog.git
```

2. Launch the application using Docker Compose in detached mode:

```
docker-compose up -d
```

3. Run database migration:

```
docker-compose exec app rails db:migrate
```

and seeds:

```
docker-compose exec app rails db:seed
```


That's it. Now you can use it. Open:

http://0.0.0.0:3000/api/v1/articles

http://0.0.0.0:3000/api/v1/articles/1

...

---
### Swagger doc available on:

http://0.0.0.0:3000/api-docs/index.html

---

To stop and remove the Docker containers, use the following command:

```
docker-compose down
```
