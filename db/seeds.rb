# frozen_string_literal: true

Article.create(title: 'The Art of Cooking', body: 'Discover the culinary secrets of expert chefs.')
Article.create(title: 'Exploring National Parks', body: 'Experience the beauty of nature in our national parks.')
Article.create(title: 'Tech Trends 2023', body: 'Stay updated with the latest tech trends and innovations.')
Article.create(title: 'Healthy Living Tips', body: 'Learn how to maintain a healthy lifestyle with our tips.')
Article.create(title: 'Travel Adventures',
               body: 'Embark on exciting adventures to the world\'s most exotic destinations.')
